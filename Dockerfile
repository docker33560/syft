FROM alpine:3.16.2
ARG SYFT_VERSION="0.60.3"
ENV URL "https://raw.githubusercontent.com/anchore/syft/main/install.sh"
RUN apk --no-cache add ca-certificates curl && \
    curl -sSfL $URL | sh -s -- -b /usr/local/bin v${SYFT_VERSION}
RUN addgroup -S syft && adduser -S syft -G syft
USER syft
ENTRYPOINT ["/usr/local/bin/syft"]
