[![linuxserver.io](https://stef33560.gitlab.io/img/avatar.png)](https://stef33560.gitlab.io/docs/docker/outillage/#action- "That's here I'm talking about it")

![](https://static.scarf.sh/a.png?x-pxid=6bc8f0dc-84c5-464a-8523-77e83a9a8d06)
# [docker33560/syft](https://gitlab.com/docker33560/syft)

[![Scarf.io pulls](https://scarf.sh/installs-badge/stef/docker33560%2Fsyft?package-type=docker)](https://scarf.sh/gateway/stef33560/docker/docker33560%2Fsyft)
[![GitLab Stars](https://img.shields.io/gitlab/stars/docker33560/syft.svg?style=for-the-badge&logo=gitlab)](https://gitlab.com/docker33560)
[![GitLab Release](https://img.shields.io/gitlab/v/release/docker33560/syft?style=for-the-badge&logo=gitlab)](https://gitlab.com/docker33560/syft/releases)
[![GitLab Container Registry](https://img.shields.io/static/v1.svg?style=for-the-badge&label=stef33560&message=GitLab%20Registry&logo=gitlab)](https://gitlab.com/docker33560/syft/container_registry)

[aquasec/syft](https://github.com/anchore/syft) est un générateur de Software Bill of Materials (SBOM) . Plus d'infos dispo sur [mon site](https://stef33560.gitlab.io/docs/docker/outillage/#action-) 

## Usage

Voici un exemple pour créer un conteneur.

```bash
docker run --rm \
  --network="host" \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v ~/.cache:/root/.cache \
  -v ./result:/tmp/
  stef.docker.scarf.sh/docker33560/syft:latest
```

## Build local

```bash
git clone https://gitlab.com/docker33560/syft.git stef33560-syft
cd stef33560-syft
docker build \
  --no-cache \
  --pull \
  --build-arg SYFT_VERSION="0.60.3" #optionnal
  -t stef.docker.scarf.sh/docker33560/syft:latest .
```

## Versions

* **11.11.22:** - Initial Release.